package address.data;

/**
 * @author Gayathiri Shriram
 * @version 1.0
 * @since Version 1.0
 * <p>
 * purpose: AddressEntry class handles getting values from the user and
 * setting them to the appropriate fields.
 */

public class AddressEntry implements Comparable<AddressEntry> {
	private String id;
	private Name name;
	private Address address;
	private String phone;
	private String email;

	/**
	 * No parameter Constructor
	 */
	public AddressEntry() {

	}

	/**
	 * Constructor with parameters.
	 *  @param firstName
	 * @param lastName
	 * @param street
	 * @param city
	 * @param state
	 * @param zipcode
	 * @param phone
	 * @param email
	 */
	public AddressEntry(String id, String firstName, String lastName, String street, String city, String state, int zipcode,
						String phone, String email) {
		this.id = id;
		this.name = new Name(firstName, lastName);
		this.address = new Address(street,city,state,zipcode);
		this.phone = phone;
		this.email = email;
	}

	/**
	 * Contstructor that accepts name and address object
	 * @param Id
	 * @param name
	 * @param address
	 * @param phone
	 * @param email
	 */
	public AddressEntry(String Id, Name name, Address address, String phone, String email) {
		this.id = Id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.email = email;
	}

	/**
	 * get id
	 * @return ID
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * set id
	 * @param Id
	 */
	public void setId(String Id)
	{
		this.id = Id;
	}

	/**
	 * get name
	 * @return name
	 */
	public Name getName() {
		return name;
	}

	/**
	 * set name
	 * @param firstName
	 * @param lastName
	 */
	public void setName(String firstName, String lastName)
	{
		name = new Name(firstName,lastName);
	}

	public Address getAddress() {
		return address;
	}

	/**
	 * set address
	 * @param street
	 * @param city
	 * @param state
	 * @param zipcode
	 */
	public void setAddress(String street, String city, String state, String zipcode) {
		address = new Address(street, city, state, Integer.parseInt(zipcode));
	}

	/**
	 * Get phone
	 * 
	 * @return phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Set phone
	 * 
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Get email
	 * 
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Set email
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * toString() is used to get a string object representing the value of
	 * the(non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getId() + "\n" + getName() + "\n" + getAddress() + "\n" + getPhone() + "\n" + getEmail();
	}

	/**
	 * compareTo used to compare and return the result.
	 * @param o
	 * @return
	 */
	@Override
	public int compareTo(AddressEntry o) {
		return name.compareTo(o.getName());
	}
}
