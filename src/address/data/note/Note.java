package address.data.note;

/**
 * @author Gayathiri Shriram
 * @version 1.0
 * @since Version 1.0
 * <p>
 * purpose: Note class handles getting content of note along with ID and setting it to corresponding field
 */

public class Note {
	private String content;
	private String addressEntryId;

	/**
	 * Note constructor
	 * @param addressEntryId
	 * @param content
	 */
	public Note(String addressEntryId, String content) {
		this.addressEntryId = addressEntryId;
		this.content = content;
	}

	/**
	 * get contect
	 * @return content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * set content
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * get Address entry id
	 * @return addressEntryId
	 */
	public String getAddressEntryId() {
		return addressEntryId;
	}

	/**
	 * set Address entry ID
	 * @param addressEntryId
	 */
	public void setAddressEntryId(String addressEntryId) {
		this.addressEntryId = addressEntryId;
	}

}
