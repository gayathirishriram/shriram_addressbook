package address.data;

/**
 * @author Gayathiri Shriram
 * @version 1.0
 * @since Version 1.0
 * <p>
 * purpose: Name class handles getting values and
 * setting them to the appropriate fields.
 */

public class Name implements Comparable<Name> {
	private String firstName;
	private String lastName;

	/**
	 * No Parameter Constructor
	 */
	public Name() {

	}

	/**
	 * Parameterized Constructor
	 * @param firstName
	 * @param lastName
	 */
	public Name(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/**
	 * get lastname
	 * @return lastname
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * set lastname
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * get firstname
	 * @return firstname
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * set firstname
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * toString() is used to get a string object representing the value of
	 * @return firstname and lastname
	 */
	@Override
	public String toString() {
		return getFirstName() + "\n" + getLastName();
	}

	/**
	 * Compare and return the result
	 * @param name
	 * @return comparison result
	 */
	@Override
	public int compareTo(Name name) {
		return lastName.compareTo(name.getLastName());
	}

}
