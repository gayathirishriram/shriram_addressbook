package address.data;

/**
 * @author Gayathiri Shriram
 * @version 1.0
 * @since Version 1.0
 * <p>
 * purpose: Address class handles getting values and
 * setting them to the appropriate fields.
 */

public class Address {
	private String street;
	private String city;
	private String state;
	private int zipcode;

	/**
	 * No parameter constructor
	 */
	public Address() {

	}

	/**
	 * Parameterized constructor
	 * @param street
	 * @param city
	 * @param state
	 * @param zipcode
	 */
	public Address(String street, String city, String state, int zipcode) {
		this.street = street;
		this.city = city;
		this.state = state;
		this.zipcode = zipcode;
	}

	/**
	 * get street
	 * @return street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * set street
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * get city
	 * @return city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * set city
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * get state
	 * @return state
	 */
	public String getState() {
		return state;
	}

	/**
	 * set state
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * get zipcode
	 * @return zipcode
	 */
	public int getZipcode() {
		return zipcode;
	}

	/**
	 * set zipcode
	 * @param zipcode
	 */
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * toString() is used to get a string object representing the value of
	 * @return street,city,state,zipcode.
	 */
	@Override
	public String toString() {
		return getStreet() + "\n" + getCity() + "\n" + getState() + "\n" + getZipcode();
	}

}
