package address;

import java.awt.EventQueue;
import address.gui.AddressBookGUI;

/**
 * @author Gayathiri Shriram
 * @version 1.0
 * @since version 1.0
 * <p>
 * purpose:
 * AddressBookApplication class is the main program.
 * The address entries are added and listed through the method in this class.
 */



public class AddressBookApplication {
    /**
     * Main method where the program execution begins.
     * @param args
     */

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddressBookGUI window = new AddressBookGUI();
					window.getFrame().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
    }
}
