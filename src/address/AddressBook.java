package address;

/**
 * @author Gayathiri Shriram
 * @version 1.0
 * @since Version 1.0
 * <p>
 * purpose:
 * AddressBook class is to create an array list of address entries and list them.
 */

import address.data.AddressEntry;
import address.data.note.Note;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class AddressBook {
	private static List<AddressEntry> addressEntryList;
	private static List<Note> noteContent;

	/**
	 * Constructor
	 */
	public AddressBook() {
		addressEntryList = new ArrayList<AddressEntry>();
		noteContent = new ArrayList<Note>();
	}

	/**
	 * List method is used to list the address entries.
	 */
	public List<AddressEntry> list() {
		Collections.sort(addressEntryList);
		return addressEntryList;
	}

	/**
	 * Add method to add the entries into the array list.
	 *
	 * @param ae
	 *            - AddressEntry object which you want to add
	 */
	public void addition(AddressEntry ae) {
		addressEntryList.add(ae);
	}

	/**
	 * Remove an address book entry
	 *
	 * @param searchString
	 *            - Search string
	 */
	public void remove(int indexToRemove) {
		addressEntryList.remove(indexToRemove);
	}

	/**
     * Add a note
	 * @param note
	 */
	public void addNote(Note note) {
		noteContent.add(note);
	}

	/**
     * Display the notes linked to each address entry
	 * @return notecontect
	 *
	 */
	public List<Note> listNote() {
		return noteContent;
	}

    /**
     * toString method for string representation of object
     * @return str
     */
    @Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		for (AddressEntry entry : addressEntryList) {
			str.append(entry.toString());
			str.append("\n");
		}
		return str.toString();
	}
}
