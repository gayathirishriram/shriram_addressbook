package address.gui;

/**
 * @author Gayathiri Shriram
 * @version 1.0
 * @since Version 1.0
 * <p>
 * purpose: AddressBookGUI class handles GUI part of the application
 */

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import address.gui.event.AddressBookHandler;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.CardLayout;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

public class AddressBookGUI {

	private AddressBookHandler addressBookHandler;
	private JFrame frame;
	private JTable displayTable;
	private JTextField textLastName;
	private JTextField textFirstName;
	private JTextField textStreet;
	private JTextField textCity;
	private JTextField textState;
	private JTextField textZip;
	private JTextField textPhone;
	private JTextField textEmail;
	private JTextField textSearch;
	private JTextField textSearchNote;
	private JTable displayNoteTable;
	private JTextField textAddressEntryId;
	private JTextField textContent;

	/**
	 * Create the application.
	 */
	public AddressBookGUI() {
		addressBookHandler = new AddressBookHandler();
		initialize();
	}

	public JFrame getFrame() {
		return frame;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
		JPanel displayPanel = new JPanel();
		frame.getContentPane().add(displayPanel, "name_1582909762705093");
		displayPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		displayPanel.add(panel_1, BorderLayout.NORTH);
		
		textSearch = new JTextField();
		panel_1.add(textSearch);
		textSearch.setColumns(10);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
            /**
             * Display Contents
             * @param e
             */
            public void actionPerformed(ActionEvent e) {
				addressBookHandler.handleFilter(textSearch, displayTable);
			}
		});
		panel_1.add(btnSearch);
		
		displayTable = new JTable();
		displayTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		displayTable.addInputMethodListener(new InputMethodListener() {
			public void caretPositionChanged(InputMethodEvent event) {
			}
			public void inputMethodTextChanged(InputMethodEvent event) {
			}
		});
		displayPanel.add(displayTable);
		displayTable.setBackground(Color.LIGHT_GRAY);
		
		JPanel panel = new JPanel();
		displayPanel.add(panel, BorderLayout.SOUTH);
		
		JButton btnNewButton_2 = new JButton("Update");
		btnNewButton_2.addActionListener(new ActionListener() {
            /**
             * Update contents
             * @param e
             */
            public void actionPerformed(ActionEvent e) {
				addressBookHandler.handleUpdate(displayTable);
			}
		});
		panel.add(btnNewButton_2);
		
		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
            /**
             * Remove content
             * @param e
             */
            public void actionPerformed(ActionEvent e) {
				addressBookHandler.handleRemove(displayTable);
			}
		});
		panel.add(btnRemove);
		
		JPanel addPanel = new JPanel();
		frame.getContentPane().add(addPanel, "name_1582909747864574");
		JPanel displayNotePanel = new JPanel();
		frame.getContentPane().add(displayNotePanel, "name_1597484333526022");

		GridBagLayout gbl_addPanel = new GridBagLayout();
		gbl_addPanel.columnWidths = new int[]{112, 112, 112, 112, 0};
		gbl_addPanel.rowHeights = new int[]{40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 0};
		gbl_addPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_addPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		addPanel.setLayout(gbl_addPanel);
		
		JLabel lblNewLabel_1 = new JLabel("First Name");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 0;
		addPanel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		textFirstName = new JTextField();
		GridBagConstraints gbc_textFirstName = new GridBagConstraints();
		gbc_textFirstName.fill = GridBagConstraints.BOTH;
		gbc_textFirstName.insets = new Insets(0, 0, 5, 5);
		gbc_textFirstName.gridx = 2;
		gbc_textFirstName.gridy = 0;
		addPanel.add(textFirstName, gbc_textFirstName);
		textFirstName.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Last Name");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 1;
		addPanel.add(lblNewLabel, gbc_lblNewLabel);
		
		textLastName = new JTextField();
		GridBagConstraints gbc_textLastName = new GridBagConstraints();
		gbc_textLastName.fill = GridBagConstraints.BOTH;
		gbc_textLastName.insets = new Insets(0, 0, 5, 5);
		gbc_textLastName.gridx = 2;
		gbc_textLastName.gridy = 1;
		addPanel.add(textLastName, gbc_textLastName);
		textLastName.setColumns(10);
		
		JLabel lblS = new JLabel("Street");
		GridBagConstraints gbc_lblS = new GridBagConstraints();
		gbc_lblS.fill = GridBagConstraints.BOTH;
		gbc_lblS.insets = new Insets(0, 0, 5, 5);
		gbc_lblS.gridx = 1;
		gbc_lblS.gridy = 2;
		addPanel.add(lblS, gbc_lblS);
		
		textStreet = new JTextField();
		GridBagConstraints gbc_textStreet = new GridBagConstraints();
		gbc_textStreet.fill = GridBagConstraints.BOTH;
		gbc_textStreet.insets = new Insets(0, 0, 5, 5);
		gbc_textStreet.gridx = 2;
		gbc_textStreet.gridy = 2;
		addPanel.add(textStreet, gbc_textStreet);
		textStreet.setColumns(10);
		
		JLabel lblCity = new JLabel("City");
		GridBagConstraints gbc_lblCity = new GridBagConstraints();
		gbc_lblCity.fill = GridBagConstraints.BOTH;
		gbc_lblCity.insets = new Insets(0, 0, 5, 5);
		gbc_lblCity.gridx = 1;
		gbc_lblCity.gridy = 3;
		addPanel.add(lblCity, gbc_lblCity);
		
		textCity = new JTextField();
		GridBagConstraints gbc_textCity = new GridBagConstraints();
		gbc_textCity.fill = GridBagConstraints.BOTH;
		gbc_textCity.insets = new Insets(0, 0, 5, 5);
		gbc_textCity.gridx = 2;
		gbc_textCity.gridy = 3;
		addPanel.add(textCity, gbc_textCity);
		textCity.setColumns(10);
		
		JLabel lblState = new JLabel("State");
		GridBagConstraints gbc_lblState = new GridBagConstraints();
		gbc_lblState.fill = GridBagConstraints.BOTH;
		gbc_lblState.insets = new Insets(0, 0, 5, 5);
		gbc_lblState.gridx = 1;
		gbc_lblState.gridy = 4;
		addPanel.add(lblState, gbc_lblState);
		
		textState = new JTextField();
		GridBagConstraints gbc_textState = new GridBagConstraints();
		gbc_textState.fill = GridBagConstraints.BOTH;
		gbc_textState.insets = new Insets(0, 0, 5, 5);
		gbc_textState.gridx = 2;
		gbc_textState.gridy = 4;
		addPanel.add(textState, gbc_textState);
		textState.setColumns(10);
		
		JLabel lblZip = new JLabel("Zip");
		GridBagConstraints gbc_lblZip = new GridBagConstraints();
		gbc_lblZip.fill = GridBagConstraints.BOTH;
		gbc_lblZip.insets = new Insets(0, 0, 5, 5);
		gbc_lblZip.gridx = 1;
		gbc_lblZip.gridy = 5;
		addPanel.add(lblZip, gbc_lblZip);
		
		textZip = new JTextField();
		GridBagConstraints gbc_textZip = new GridBagConstraints();
		gbc_textZip.fill = GridBagConstraints.BOTH;
		gbc_textZip.insets = new Insets(0, 0, 5, 5);
		gbc_textZip.gridx = 2;
		gbc_textZip.gridy = 5;
		addPanel.add(textZip, gbc_textZip);
		textZip.setColumns(10);
		
		JLabel lblPhone = new JLabel("Phone");
		GridBagConstraints gbc_lblPhone = new GridBagConstraints();
		gbc_lblPhone.fill = GridBagConstraints.BOTH;
		gbc_lblPhone.insets = new Insets(0, 0, 5, 5);
		gbc_lblPhone.gridx = 1;
		gbc_lblPhone.gridy = 6;
		addPanel.add(lblPhone, gbc_lblPhone);
		
		textPhone = new JTextField();
		GridBagConstraints gbc_textPhone = new GridBagConstraints();
		gbc_textPhone.fill = GridBagConstraints.BOTH;
		gbc_textPhone.insets = new Insets(0, 0, 5, 5);
		gbc_textPhone.gridx = 2;
		gbc_textPhone.gridy = 6;
		addPanel.add(textPhone, gbc_textPhone);
		textPhone.setColumns(10);
		
		JLabel lblEmail_1 = new JLabel("Email");
		GridBagConstraints gbc_lblEmail_1 = new GridBagConstraints();
		gbc_lblEmail_1.fill = GridBagConstraints.BOTH;
		gbc_lblEmail_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmail_1.gridx = 1;
		gbc_lblEmail_1.gridy = 7;
		addPanel.add(lblEmail_1, gbc_lblEmail_1);
		
		textEmail = new JTextField();
		GridBagConstraints gbc_textEmail = new GridBagConstraints();
		gbc_textEmail.fill = GridBagConstraints.BOTH;
		gbc_textEmail.insets = new Insets(0, 0, 5, 5);
		gbc_textEmail.gridx = 2;
		gbc_textEmail.gridy = 7;
		addPanel.add(textEmail, gbc_textEmail);
		textEmail.setColumns(10);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() {
            /**
             * Adding contacts
             * @param e
             */
            public void actionPerformed(ActionEvent e) {
				addressBookHandler.handleAddAddress(textFirstName, textLastName, 
						textStreet, textCity, textState, textZip, textPhone, textEmail);
			}
		});
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton.gridx = 2;
		gbc_btnNewButton.gridy = 9;
		addPanel.add(btnNewButton, gbc_btnNewButton);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnAddress = new JMenu("Address");
		menuBar.add(mnAddress);
		
		JMenuItem mntmNew = new JMenuItem("New");
		mntmNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPanel.setVisible(false);
				displayNotePanel.setVisible(false);
				addPanel.setVisible(true);
			}
		});
		mnAddress.add(mntmNew);
		
		JMenuItem mntmDisplay = new JMenuItem("Display");
		mntmDisplay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPanel.setVisible(false);
				displayNotePanel.setVisible(false);
				displayPanel.setVisible(true);

				addressBookHandler.handleDisplayAddress(displayTable);
			}
		});
		mnAddress.add(mntmDisplay);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.addActionListener(new ActionListener() {
            /**
             * Saving Contacts
             * @param e
             */
            public void actionPerformed(ActionEvent e) {
				addressBookHandler.handleSave();
			}
		});
		
		JMenuItem mntmOtes = new JMenuItem("Notes");
		mntmOtes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPanel.setVisible(false);
				displayPanel.setVisible(false);
				displayNotePanel.setVisible(true);
				addressBookHandler.handleDisplayNote(displayNoteTable);
			}
		});
		mnAddress.add(mntmOtes);
		mnAddress.add(mntmSave);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
            /**
             * Exit Application
             * @param e
             */
            public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		menuBar.add(mntmExit);
		addressBookHandler.handleDisplayAddress(displayTable);
		
		displayNotePanel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		displayNotePanel.add(panel_2, BorderLayout.NORTH);
		
		textSearchNote = new JTextField();
		panel_2.add(textSearchNote);
		textSearchNote.setColumns(10);
		
		JButton btnSearch_1 = new JButton("Search");
		btnSearch_1.addActionListener(new ActionListener() {
            /**
             * Searching Note
             * @param e
             */
            public void actionPerformed(ActionEvent e) {
				addressBookHandler.handleFilterNote(textSearchNote, displayNoteTable);
			}
		});
		panel_2.add(btnSearch_1);
		
		displayNoteTable = new JTable();
		displayNoteTable.setBackground(Color.LIGHT_GRAY);
		displayNotePanel.add(displayNoteTable, BorderLayout.CENTER);
		addressBookHandler.handleDisplayNote(displayNoteTable);
		
		JPanel panel_3 = new JPanel();
		displayNotePanel.add(panel_3, BorderLayout.SOUTH);
		
		JLabel lblId = new JLabel("Id");
		panel_3.add(lblId);
		
		textAddressEntryId = new JTextField();
		panel_3.add(textAddressEntryId);
		textAddressEntryId.setColumns(10);
		
		JLabel lblNotes = new JLabel("Notes");
		panel_3.add(lblNotes);
		
		textContent = new JTextField();
		panel_3.add(textContent);
		textContent.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
            /**
             * Adding Note
             * @param e
             */
            public void actionPerformed(ActionEvent e) {
				addressBookHandler.handleNoteAddition(textAddressEntryId, textContent, displayNoteTable);
			}
		});
		panel_3.add(btnAdd);
	}
}
