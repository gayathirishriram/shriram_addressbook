package address.gui.event;

/**
 * @author Gayathiri Shriram
 * @version 1.0
 * @since Version 1.0
 * <p>
 * purpose: AddressbookHadler class handles all events like button press,selection mode
 *
 */

import address.AddressBook;
import address.data.AddressEntry;
import address.data.note.Note;

import java.io.FileReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.io.*;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;


public class AddressBookHandler {
	private Connection conn;
	AddressBook addressBook;
	Random rand = new Random();

	/**
	 * Constructor
	 */
	public AddressBookHandler() {
		try {
			conn = DriverManager
					.getConnection("jdbc:oracle:thin:ms5754/EPfloQav@mcsdb1.sci.csueastbay.edu:1521/MCSDB1");
			addressBook = new AddressBook();
			String sql = "select * from ADDRESSENTRYTABLE";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String id = rs.getString("id");
				String firstName = rs.getString("firstName");
				String lastName = rs.getString("lastName");
				String street = rs.getString("street");
				String city = rs.getString("city");
				String state = rs.getString("state");
				int zip = rs.getInt("zip");
				String phone = rs.getString("phone");
				String email = rs.getString("email");
				AddressEntry ae = new AddressEntry(id, firstName, lastName, street, city, state, zip, phone, email);
				addressBook.addition(ae);
			}
			rs.close();

			sql = "select * from NOTESTABLE";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String id = rs.getString("addressentryid");
				String content = rs.getString("content");
				Note note = new Note(id, content);
				addressBook.addNote(note);
			}
			rs.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Display notes
	 * @param displayNoteTable
	 */
	public void handleDisplayNote(JTable displayNoteTable) {
		List<Note> entries = addressBook.listNote();
		populateNoteTable(displayNoteTable, entries);
	}

	/**
	 * getting the notes from database
	 * @param displayTable
	 * @param entries
	 */
	private void populateNoteTable(JTable displayTable, List<Note> entries) {
		String cols[] = new String[] { "Address Book Id", "Content" };
		TableModel model = new DefaultTableModel(cols, entries.size());
		int row = 0;
		for (Note note : entries) {
			model.setValueAt(note.getAddressEntryId(), row, 0);
			model.setValueAt(note.getContent(), row, 1);
			row++;
		}
		displayTable.setModel(model);
	}

	/**
	 * Display the contacts
	 * @param displayTable
	 */
	public void handleDisplayAddress(JTable displayTable) {
		List<AddressEntry> entries = addressBook.list();
		populateTable(displayTable, entries);
	}

	/**
	 * Getting the details from database and populating it on GUI
	 * @param displayTable
	 * @param entries
	 */
	private void populateTable(JTable displayTable, List<AddressEntry> entries) {
		String cols[] = new String[] { "Id", "First Name", "Last Name", "Street", "City", "State", "Zip", "Phone",
				"Email" };
		TableModel model = new DefaultTableModel(cols, entries.size());
		int row = 0;
		for (AddressEntry entry : entries) {
			model.setValueAt(entry.getId(), row, 0);
			model.setValueAt(entry.getName().getFirstName(), row, 1);
			model.setValueAt(entry.getName().getLastName(), row, 2);
			model.setValueAt(entry.getAddress().getStreet(), row, 3);
			model.setValueAt(entry.getAddress().getCity(), row, 4);
			model.setValueAt(entry.getAddress().getState(), row, 5);
			model.setValueAt(Integer.toString(entry.getAddress().getZipcode()), row, 6);
			model.setValueAt(entry.getPhone(), row, 7);
			model.setValueAt(entry.getEmail(), row, 8);
			row++;
		}
		displayTable.setModel(model);
	}

	/**
	 * Inserting new contact into addressbook
	 * @param firstName
	 * @param lastName
	 * @param street
	 * @param city
	 * @param state
	 * @param zip
	 * @param phone
	 * @param email
	 */
	public void handleAddAddress(JTextField firstName, JTextField lastName, JTextField street, JTextField city,
			JTextField state, JTextField zip, JTextField phone, JTextField email) {

		AddressEntry ae = new AddressEntry("", firstName.getText(), lastName.getText(), street.getText(),
				city.getText(), state.getText(), Integer.parseInt(zip.getText()), phone.getText(), email.getText());
		addressBook.addition(ae);
		// Clear all fields
		firstName.setText("");
		lastName.setText("");
		street.setText("");
		city.setText("");
		state.setText("");
		zip.setText("");
		phone.setText("");
		email.setText("");
	}

	/**
	 * Saving new changes made to the address Book
	 */
	public void handleSave() {
		List<AddressEntry> entries = addressBook.list();
		for (AddressEntry entry : entries) {
			String sql;
			try {
				if (entry.getId().isEmpty()) {
					sql = String.format(
							"INSERT INTO ADDRESSENTRYTABLE (\"Id\",\"FirstName\",\"LastName\",\"Street\",\"City\",\"State\",\"Zip\",\"Phone\",\"Email\") VALUES ('%s','%s','%s','%s','%s','%s',%d,'%s','%s')",
							String.valueOf(rand.nextInt(100000)), entry.getName().getFirstName(),
							entry.getName().getLastName(), entry.getAddress().getStreet(), entry.getAddress().getCity(),
							entry.getAddress().getState(), entry.getAddress().getZipcode(), entry.getPhone(),
							entry.getEmail());

					Statement stmt = conn.createStatement();
					stmt.setQueryTimeout(10);
					int rowsAffected = stmt.executeUpdate(sql);
					if (rowsAffected < 1) {
						// Print error
					}
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Searching the address book
	 * @param search
	 * @param displayTable
	 */
	public void handleFilter(JTextField search, JTable displayTable) {
		List<AddressEntry> result = new ArrayList<AddressEntry>();
		String searchString = search.getText();
		for (AddressEntry addressEntry : addressBook.list()) {
			if (addressEntry.getName().getLastName().matches(searchString + "(.*)")) {
				result.add(addressEntry);
			}
		}
		populateTable(displayTable, result);
		search.setText("");
	}

	/**
	 * Removing an entry from address book
	 * @param displayTable
	 */
	public void handleRemove(JTable displayTable) {
		DefaultTableModel model = (DefaultTableModel) displayTable.getModel();
		int index = displayTable.getSelectedRow();
		model.removeRow(index);
		addressBook.remove(index);
	}

	/**
	 * Updating the address book
	 * @param displayTable
	 */
	public void handleUpdate(JTable displayTable) {
		DefaultTableModel model = (DefaultTableModel) displayTable.getModel();
		int row = displayTable.getSelectedRow();
		String id = (String) model.getValueAt(row, 0);
		String firstName = (String) model.getValueAt(row, 1);
		String lastName = (String) model.getValueAt(row, 2);
		String street = (String) model.getValueAt(row, 3);
		String city = (String) model.getValueAt(row, 4);
		String state = (String) model.getValueAt(row, 5);
		String zip = (String) model.getValueAt(row, 6);
		String phone = (String) model.getValueAt(row, 7);
		String email = (String) model.getValueAt(row, 8);
		AddressEntry ae = new AddressEntry(id, firstName, lastName, street, city, state, Integer.parseInt(zip), phone,
				email);
		addressBook.remove(row);
		addressBook.addition(ae);
	}

	/**
	 * Adding new notes to contact
	 * @param id
	 * @param content
	 * @param displayNoteTable
	 */
	public void handleNoteAddition(JTextField id, JTextField content, JTable displayNoteTable) {
		String addressBookEntryId = id.getText();
		String noteContent = content.getText();
		Note note = new Note(addressBookEntryId, noteContent);
		addressBook.addNote(note);
		handleDisplayNote(displayNoteTable);
		String sql = String.format("INSERT INTO NOTESTABLE (\"ADDRESSENTRYID\",\"CONTENT\") VALUES ('%s','%s')",
				addressBookEntryId, noteContent);
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.setQueryTimeout(10);
			int rowsAffected = stmt.executeUpdate(sql);
			if (rowsAffected < 1) {
				// Print error
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		id.setText("");
		content.setText("");
	}

	/**
	 * Searching for notes
	 * @param search
	 * @param displayNoteTable
	 */
	public void handleFilterNote(JTextField search, JTable displayNoteTable) {
		List<Note> result = new ArrayList<Note>();
		String searchString = search.getText();
		String sql = "select * from NOTESTABLE where CONTENT like '%" + searchString + "%'";
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String id = rs.getString("addressentryid");
				String content = rs.getString("content");
				Note note = new Note(id, content);
				result.add(note);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		populateNoteTable(displayNoteTable, result);
		search.setText("");
	}

}
