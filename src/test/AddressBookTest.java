/**
 * 
 */
package test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import address.AddressBook;
import address.data.AddressEntry;

/**
 * @author Gaya
 *
 */
public class AddressBookTest {
	
	private static AddressBook addressBook;
    static AddressEntry addressEntry1;
    static AddressEntry addressEntry2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
	  addressBook = new AddressBook();
      addressEntry1 = new AddressEntry("101","Gaya", "Bob", "12 St", "Fremont", "CA", 94555, "1234567890", "Gaya@Shriram.com");
      addressEntry2 = new AddressEntry("102","Bala", "Alice", "45 St", "Sunnyvale", "AZ", 94089, "9876543210", "Bala@Sankar.com");
      addressBook.addition(addressEntry1);
      addressBook.addition(addressEntry2);
	}


	/**
	 * Test method for {@link address.AddressBook#list()}.
	 */
	@Test
	public void testList() {
		StringBuffer expected = new StringBuffer();
        expected.append(addressEntry1.toString()).append("\n").append(addressEntry2.toString()).append("\n");
        Assert.assertEquals("Unsorted - Address book matches", expected.toString(), addressBook.toString());
        addressBook.list();
        expected = new StringBuffer();
        expected.append(addressEntry2.toString()).append("\n").append(addressEntry1.toString()).append("\n");
        Assert.assertEquals("Sorted - Address book matches", expected.toString(), addressBook.toString());
	}

	/**
	 * Test method for {@link address.AddressBook#addition(address.data.AddressEntry)}.
	 */
	@Test
	public void testAddition() {
		StringBuffer expected = new StringBuffer();
		expected.append(addressEntry1.toString()).append("\n").append(addressEntry2.toString()).append("\n");
		Assert.assertEquals("Address book matches", expected.toString(), addressBook.toString());
	}



	/**
	 * Test method for {@link address.AddressBook#addNote(address.data.note.Note)}.
	 */
//	@Test
//	public void testAddNote() {
//		fail("Not yet implemented");
//	}
//
//	/**
//	 * Test method for {@link address.AddressBook#listNote()}.
//	 */
//	@Test
//	public void testListNote() {
//		fail("Not yet implemented");
//	}

	/**
	 * Test method for {@link address.AddressBook#toString()}.
	 */
	@Test
	public void testToString() {
		StringBuffer expected = new StringBuffer();
		expected.append(addressEntry1.toString()).append("\n").append(addressEntry2.toString()).append("\n");
		Assert.assertEquals("Address book matches", expected.toString(), addressBook.toString());
	}

}
