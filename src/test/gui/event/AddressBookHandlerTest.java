package test.gui.event;

import org.junit.Before;

import address.AddressBook;
import address.data.AddressEntry;
import address.data.note.Note;
import junit.framework.TestCase;

/**
 * Created by Gaya on 5/17/16.
 */
public class AddressBookHandlerTest extends TestCase {
	
	private static AddressBook addressBook;
    static AddressEntry addressEntry1;
    static AddressEntry addressEntry2;
    static Note note1;
    static Note note2;
	
	@Before
	public void setup()
	{
		  addressBook = new AddressBook();
	      addressEntry1 = new AddressEntry("101","Gaya", "Bob", "12 St", "Fremont", "CA", 94555, "1234567890", "Gaya@Shriram.com");
	      addressEntry2 = new AddressEntry("102","Bala", "Alice", "45 St", "Sunnyvale", "AZ", 94089, "9876543210", "Bala@Sankar.com");
	      addressBook.addition(addressEntry1);
	      addressBook.addition(addressEntry2);
	      note1 = new Note("101","Meeting tomorrow at 10");
	      note2 = new Note("102","Meeting Cancelled");
	}
    public void testHandleDisplayNote() throws Exception {

    }

    public void testHandleDisplayAddress() throws Exception {
    	
    	
    	
    }

    public void testHandleAddAddress() throws Exception {

    	addressEntry1 = new AddressEntry("101","Gaya", "Bob", "12 St", "Fremont", "CA", 94555, "1234567890", "Gaya@Shriram.com");
	    addressEntry2 = new AddressEntry("102","Bala", "Alice", "45 St", "Sunnyvale", "AZ", 94089, "9876543210", "Bala@Sankar.com");
	    
    }

    public void testHandleSave() throws Exception {

    }

    public void testHandleFilter() throws Exception {

    }

    public void testHandleRemove() throws Exception {

    }

    public void testHandleUpdate() throws Exception {

    }

    public void testHandleNoteAddition() throws Exception {

    }

    public void testHandleFilterNote() throws Exception {

    }

}