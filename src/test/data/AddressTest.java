package test.data;

import address.data.Address;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;

    /**
     * Created by Gaya on 5/16/16.
     */
    public class AddressTest extends TestCase {

        private Address address1;
        private Address address2;

        @Before
        public void setUp() {
            address1 = new Address("Deepcreek","Fremont","CA",94555);
            address2 = new Address("Seaward","UnionCity","CA",94555);
        }


    public void testGetStreet() throws Exception {

        Assert.assertEquals("Street matched", "Deepcreek", address1.getStreet());
        Assert.assertEquals("Street matched", "Seaward", address2.getStreet());

    }

    public void testSetStreet() throws Exception {

        String street = "Deepcreek";
        address1.setStreet(street);
        Assert.assertEquals("Street matches", street, address1.getStreet());
        street = "seaward";
        address2.setStreet(street);
        Assert.assertEquals("Street matches", street, address2.getStreet());

    }

    public void testGetCity() throws Exception {

        Assert.assertEquals("City matched", "Fremont", address1.getCity());
        Assert.assertEquals("City matched", "UnionCity", address2.getCity());
    }

    public void testSetCity() throws Exception {

        String city = "Fremont";
        address1.setCity(city);
        Assert.assertEquals("City matches", city, address1.getCity());
        city = "UnionCity";
        address2.setCity(city);
        Assert.assertEquals("City matches", city, address2.getCity());
    }

    public void testGetState() throws Exception {

        Assert.assertEquals("State matched", "CA", address1.getState());
        Assert.assertEquals("State matched", "CA", address2.getState());

    }

    public void testSetState() throws Exception {

        String state = "CA";
        address1.setState(state);
        Assert.assertEquals("State matches", state, address1.getState());
        state = "CA";
        address2.setState(state);
        Assert.assertEquals("State matches", state, address2.getState());

    }

    public void testGetZipcode() throws Exception {

        Assert.assertEquals("Zipcode matched",94555, address1.getZipcode());
        Assert.assertEquals("ZipCode matched",94555, address2.getZipcode());

    }

    public void testSetZipcode() throws Exception {

        int zip = 94555;
        address1.setZipcode(zip);
        Assert.assertEquals("Zipcode matches", zip, address1.getZipcode());
        zip = 94555;
        address2.setZipcode(zip);
        Assert.assertEquals("Zipcode matches", zip, address2.getZipcode());

    }

    public void testToString() throws Exception {

        Assert.assertTrue("ToString works", address1.toString().contains("Deepcreek"));
        Assert.assertTrue("ToString works", address2.toString().contains("Seaward"));

    }

}