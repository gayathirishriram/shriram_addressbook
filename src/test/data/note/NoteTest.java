package test.data.note;

import address.data.note.Note;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;

/**
 * Created by Gaya on 5/16/16.
 */
public class NoteTest extends TestCase {

    private Note note1;
    private Note note2;

    @Before
    public void setUp() {
        note1 = new Note("101","Meeting Tomorrow at 10 AM");
        note2 = new Note("102","Meeting Cancelled");
    }

    public void testGetContent() throws Exception {
        Assert.assertEquals("Content matched", "Meeting Tomorrow at 10 AM", note1.getContent());
        Assert.assertEquals("Content matched", "Meeting Cancelled", note2.getContent());

    }

    public void testSetContent() throws Exception {

        String content = "Meeting Tomorrow at 10 AM";
        note1.setContent(content);
        Assert.assertEquals("Content matches", content, note1.getContent());
        content = "Meeting Cancelled";
        note2.setContent(content);
        Assert.assertEquals("Content matches", content, note2.getContent());

    }

    public void testGetAddressEntryId() throws Exception {


        Assert.assertEquals("AddressEntryID matched", "101", note1.getAddressEntryId());
        Assert.assertEquals("AddressEntryID matched", "102", note2.getAddressEntryId());

    }

    public void testSetAddressEntryId() throws Exception {

        String id = "101";
        note1.setAddressEntryId(id);
        Assert.assertEquals("AddressEntryID matches", id, note1.getAddressEntryId());
        id = "102";
        note2.setContent(id);
        Assert.assertEquals("AddressEntryID matches", id, note2.getAddressEntryId());

    }

}