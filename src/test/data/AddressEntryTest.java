/**
 *
 */
package test.data;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import address.data.AddressEntry;

/**
 * @author Gaya
 */
public class AddressEntryTest {

    private AddressEntry addressEntry1, addressEntry2;

    @Before
    public void setUp() {
        addressEntry1 = new AddressEntry("101","Gaya", "Shriram", "12 St", "Fremont", "CA", 94555, "1234567890", "Gaya@Shriram.com");
        addressEntry2 = new AddressEntry("102","Bala", "Sankar", "45 St", "Sunnyvale", "AZ", 94089, "9876543210", "Bala@Sankar.com");
    }

    @Test
    public void testAddressEntryWithParams() {
        AddressEntry addressEntry3 = new AddressEntry("101","Gaya", "Shriram", "12 St", "Fremont", "CA", 94555, "1234567890", "Gaya@Shriram.com");
        AddressEntry addressEntry4 = new AddressEntry("102","Bala", "Sankar", "45 St", "Sunnyvale", "AZ", 94089, "9876543210", "Bala@Sankar.com");
        Assert.assertEquals("Address Entry macthes", addressEntry1.toString(), addressEntry3.toString());
        Assert.assertEquals("Address Entry macthes", addressEntry2.toString(), addressEntry4.toString());
    }

    /**
     * Test method for {@link AddressEntry#getPhone()}.
     */
    @Test
    public void testGetPhone() {

        Assert.assertEquals("Phone matched", "1234567890", addressEntry1.getPhone());
        Assert.assertEquals("Phone matched", "9876543210", addressEntry2.getPhone());
    }

    /**
     * Test method for {@link AddressEntry#setPhone(java.lang.String)}.
     */
    @Test
    public void testSetPhone() {
        String phone = "11111111";
        addressEntry1.setPhone(phone);
        Assert.assertEquals("Phone matches", phone, addressEntry1.getPhone());
        addressEntry2.setPhone(phone);
        Assert.assertEquals("Phone matches", phone, addressEntry2.getPhone());
    }

    /**
     * Test method for {@link AddressEntry#getEmail()}.
     */
    @Test
    public void testGetEmail() {
        Assert.assertEquals("Phone matched", "Gaya@Shriram.com", addressEntry1.getEmail());
        Assert.assertEquals("Phone matched", "Bala@Sankar.com", addressEntry2.getEmail());
    }

    /**
     * Test method for {@link AddressEntry#setEmail(java.lang.String)}.
     */
    @Test
    public void testSetEmail() {
        String email = "ab@xyz.com";
        addressEntry1.setEmail(email);
        Assert.assertEquals("Email matches", email, addressEntry1.getEmail());
        addressEntry2.setEmail(email);
        Assert.assertEquals("Email matches", email, addressEntry2.getEmail());
    }

    /**
     * Test method for {@link AddressEntry#toString()}.
     */
    @Test
    public void testToString() {
        Assert.assertTrue("ToString works", addressEntry1.toString().contains("Gaya"));
        Assert.assertTrue("ToString works", addressEntry2.toString().contains("Bala"));
    }

    /**
     * Test method for {@link AddressEntry#compareTo(AddressEntry)}.
     */
    @Test
    public void testCompareTo() {
        Assert.assertTrue("Compare works", addressEntry1.compareTo(addressEntry2) > 0);
    }

}
