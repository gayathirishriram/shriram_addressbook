package test.data;


import address.data.Name;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;

/**
 * Created by Gaya on 5/16/16.
 */
public class NameTest extends TestCase {

    private Name name1;
    private Name name2;

    @Before
    public void setUp() {
        name1 = new Name("Bala","Sankar");
        name2 = new Name("Gaya","Shriram");
    }
    public void testGetLastName() throws Exception {

        Assert.assertEquals("LastName matched", "Sankar", name1.getLastName());
        Assert.assertEquals("LastName matched", "Shriram",name2.getLastName());

    }

    public void testSetLastName() throws Exception {

        String name = "Sankar";
        name1.setLastName(name);
        Assert.assertEquals("LastName matches", name, name1.getLastName());
        name = "Shriram";
        name2.setLastName(name);
        Assert.assertEquals("LastName matches", name, name2.getLastName());

    }

    public void testGetFirstName() throws Exception {

        Assert.assertEquals("FirstName matched", "Bala", name1.getFirstName());
        Assert.assertEquals("FirstName matched", "Gaya",name2.getFirstName());
    }

    public void testSetFirstName() throws Exception {

        String name = "Bala";
        name1.setFirstName(name);
        Assert.assertEquals("FirstName matches", name, name1.getFirstName());
        name = "Gaya";
        name2.setFirstName(name);
        Assert.assertEquals("FirstName matches", name, name2.getFirstName());

    }

    public void testToString() throws Exception {

        Assert.assertTrue("ToString works", name1.toString().contains("Sankar"));
        Assert.assertTrue("ToString works", name2.toString().contains("Gaya"));

    }

}